# MazeBallFall

MazeBallFall is a game where you have to navigate through a maze and once you reach the other side, you need to make a red ball fall off a platform. In level 2, there is a room with balls that you can hit and push to bring them to the edge of the platform and make them fall. Each fallen ball makes the walls intangible for 10 seconds, which allows you to take a shorter path to exit the maze. However, be careful because the walls do not become invisible, and you can fall off the platform by crossing an outer wall.

The game is compiled for Windows.

## Play

To play the game on Windows you can download the MazeBallFall directory and run the MazeBallFall.exe. To play on Linux you'll have to compile the game manually.

## The controls:
You move the ball using the arrow keys or W, A, S, D (Z, Q, S, D probably won't work on AZERTY keyboards). The direction of your movement and your gaze is controlled with the mouse.
